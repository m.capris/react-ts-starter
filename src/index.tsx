import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import { routes } from "./routes";

ReactDOM.render(
    <BrowserRouter children={routes} />,
    document.getElementById("root"),
);
