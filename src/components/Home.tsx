import * as React from "react";

export const Home: React.SFC<{}> = (props) => {
    return (
        <div>
            <h1>This is the home page</h1>
        </div>
    );
};
