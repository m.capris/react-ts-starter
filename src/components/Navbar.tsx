import * as React from "react";
import { LinkProps, NavLink } from "react-router-dom";

export const Navbar = () => {
    return (
        <div className="ui secondary pointing menu">
            <NavLink to="/" className="item" exact={true} activeClassName="active item">Home</NavLink>
            <NavLink to="/hello" className="item" exact={true} activeClassName="active item">Hello</NavLink>
            <div className="right menu">
                <a className="ui item">
                    Pointless buton</a>
            </div>
        </div>
    );
};
