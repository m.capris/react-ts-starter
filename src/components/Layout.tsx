import * as React from "react";
import {match, RouteComponentProps} from "react-router-dom";

import { Navbar } from "./Navbar";

interface ILayoutProps {
    children?: React.ReactNode;
}

type LayoutProps = RouteComponentProps<ILayoutProps>;

export const Layout: React.SFC<ILayoutProps> = (props) => {
    return (
        <div className="container">
            <Navbar />
            {props.children}
        </div>
    );
};
