import * as React from "react";
import { Route } from "react-router-dom";
import { Hello } from "./components/Hello";
import { Home } from "./components/Home";
import { Layout } from "./components/Layout";

export const routes = (
    <Layout>
            <Route path="/" exact={true} component={Home} />
            <Route path="/hello" exact={true} component={Hello}/>
    </Layout>
);
