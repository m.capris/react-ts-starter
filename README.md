# React TS Starter

This is a template for developing React + TS apps :)


### Prerequisites

In order to run the project you need to have NPM & nodejs installed


### Installing, Running, Deployment

just run npm install

```
to run the project you just type npm start, or if you have webpack installed globally type: webpack-dev-server
```

to get a production build type type webpack or npm run deploy

## Built With

* [Typescript](https://www.typescriptlang.org/)
* [Webpack](https://webpack.github.io/)
* [React](https://rometools.github.io/rome/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
